# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU NXP LPC54616J512BD100.

# Projekt PCB

Schemat: [doc/LPC54616_EVB_V1_0_SCH.pdf](doc/LPC54616_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/LPC54616_EVB_V1_0_3D.pdf](doc/LPC54616_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

## Top layer
![](doc/foto/PCB-altium-top.png "Top layer")

## Top assembly
![](doc/foto/PCB-altium-assembly.jpg "Top assembly")

## 3D preview
![](doc/foto/PCB-altium-3D.jpg "3D preview")

# Wyprodukowane PCB

## PCB top bez elementów

![](doc/foto/PCB-bare.jpg "PCB bare")

## PCB zmontowana

![](doc/foto/PCB-assembled.jpg "PCB assembled")

# Licencja

MIT